import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by eidonfiloi on 29/05/14.
 */

@RunWith(JUnit4.class)
public class TestShifted {

    private static Logger logger = Logger.getLogger(TestShifted.class);

    private static BufferedReader readerOne;

    private static BufferedReader readerTwo;

    private boolean readInProcessLogfiles(String fileOne, String fileTwo) throws IOException {

        boolean foundBadPairs = false;
        try {
            readerOne = new BufferedReader(new FileReader(fileOne));
            readerTwo = new BufferedReader(new FileReader(fileTwo));

            ArrayList<HashSet<String>> seriesOne = constructSeriesTest(readerOne);
            ArrayList<HashSet<String>> seriesTwo = constructSeriesTest(readerTwo);

            logger.info(seriesOne);
            logger.info(seriesTwo);

            foundBadPairs = ProcessShiftedLogs.searchForBadPairs(seriesOne,seriesTwo);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if(readerOne != null) {
                readerOne.close();
            }
            if(readerTwo != null) {
                readerTwo.close();
            }

        }
        return foundBadPairs;
    }

    private ArrayList<HashSet<String>> constructSeriesTest(BufferedReader reader) throws IOException, IllegalArgumentException, IllegalStateException {

        ArrayList<HashSet<String>> result = new ArrayList<HashSet<String>>();
        HashSet<String> last = new HashSet<String>();
        result.add(new HashSet<String>(last));
        String line;
        while((line = reader.readLine()) != null) {

            String[] lineArr = line.split(",");

            if(lineArr.length != 2) {
                throw new IllegalArgumentException("There is either delimiter problem or a malformed row...");
            }

            if(lineArr[0].equals("ACQUIRE")) {
                boolean canBeAdded = last.add(lineArr[1]);
                if(!canBeAdded) {
                    throw new IllegalStateException("SELFLOCK was found...");
                }
            } else if(lineArr[0].equals("RELEASE")) {
                last.remove(lineArr[1]);
            } else {
                throw new IllegalStateException("Logfile has unknown action...It should be either ACQUIRE or RELEASE!");
            }

            result.add(new HashSet<String>(last));

        }

        reader.close();

        return result;

    }

    @Test
    public void testDeadLock() {

        logger.info("test for deadlock...");

        try {
            boolean foundDeadlock = readInProcessLogfiles("src/main/resources/deadlock1.txt", "src/main/resources/deadlock2.txt");
            org.junit.Assert.assertTrue("should be found deadlock", foundDeadlock);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testShiftedDeadLock() {

        logger.info("test for shifted deadlock...");

        try {
            boolean foundDeadlock = readInProcessLogfiles("src/main/resources/shifted1.txt", "src/main/resources/shifted2.txt");
            org.junit.Assert.assertTrue("should be found deadlock", foundDeadlock);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFinish() {

        logger.info("test for finish...");

        try {
            boolean foundDeadlock = readInProcessLogfiles("src/main/resources/finish1.txt", "src/main/resources/finish2.txt");
            org.junit.Assert.assertFalse("should be finished...", foundDeadlock);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
