import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 5/28/14
 * Time: 3:08 PM
 */

public class TestAll {

    private static Logger logger = Logger.getLogger(TestAll.class);

    private static BufferedReader readerOne;

    private static BufferedReader readerTwo;

    private State readInProcessLogfiles(String fileOne, String fileTwo) throws IOException {

        State state = new State();
        try {
            readerOne = new BufferedReader(new FileReader(fileOne));
            readerTwo = new BufferedReader(new FileReader(fileTwo));

            CurrentState currentState;
            boolean stillContinue = true;
            int iter = 0;

            while(stillContinue) {

                /**
                 * In each iteration we call the processState method of the State object
                 * If there is still lines to read in from either logfiles, these are passed to the processState method.
                 *
                 * The method gives back the current state of the resource allocation process.
                 * There is either found a DEADLOCK or a SELFLOCK
                 * or both threads are finished without finding any of the above locks
                 * or the threads are not yet finished and the process will CONTINUE
                 *
                 * The State object does not verify the validity of the logfiles, i.e.
                 * if the given series of allocation events are realizable or not, except
                 * if there is a malformed row in any of the logfiles an Exception is thrown
                 */


                String lineOne = readerOne.readLine();
                String lineTwo = readerTwo.readLine();

                String[] lineArrOne = lineOne == null ? new String[0] : lineOne.split(",");
                String[] lineArrTwo = lineTwo == null ? new String[0] : lineTwo.split(",");

                if((lineOne != null && lineArrOne.length != 2) || (lineTwo != null && lineArrTwo.length != 2)) {

                    throw new IllegalArgumentException("There is either delimiter problem or a malformed row...");
                }

                state.processState(lineArrOne, lineArrTwo);

                switch (state.getCurrentSate()) {
                    case DEADLOCK:
                        logger.info("DEADLOCK was found...");
                        logger.info(state);
                        stillContinue = false;
                        break;
                    case SELFLOCK:
                        logger.info("SELFLOCK was found...");
                        logger.info(state);
                        stillContinue = false;
                        break;
                    case FINISHED:
                        logger.info("FINISHED AND NO DEADLOCK...");
                        stillContinue = false;
                        break;
                    case CONTINUE:
                        logger.info("CONTINUE RESOURCE ALLOCATION PROCESS " + iter++);
                        break;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if(readerOne != null) {
                readerOne.close();
            }
            if(readerTwo != null) {
                readerTwo.close();
            }

        }
        return state;
    }


    @Ignore
    public void testDeadLock() {

        logger.info("test for deadlock...");

        try {
            State state = readInProcessLogfiles("src/main/resources/deadlock1.txt", "src/main/resources/deadlock2.txt");
            org.junit.Assert.assertTrue("should be found deadlock", state.getCurrentSate().equals(CurrentState.DEADLOCK));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Ignore
    public void testSelfLock() {

        logger.info("test for self lock...");

        try {
            State state = readInProcessLogfiles("src/main/resources/selflock1.txt", "src/main/resources/selflock2.txt");
            org.junit.Assert.assertTrue("should be found selflock", state.getCurrentSate().equals(CurrentState.SELFLOCK));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Ignore
    public void testFinish() {

        logger.info("test for finish...");

        try {
            State state = readInProcessLogfiles("src/main/resources/finish1.txt", "src/main/resources/finish2.txt");
            org.junit.Assert.assertTrue("should be finished without finding locks", state.getCurrentSate().equals(CurrentState.FINISHED));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
