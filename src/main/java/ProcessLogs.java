import org.apache.commons.cli.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 5/27/14
 * Time: 12:16 PM
 *
 * Main Class
 */
public class ProcessLogs {

    public static final Logger logger = Logger.getLogger(ProcessLogs.class);

    private static String DELIMITER;

    private static String logFileOne;

    private static String logFileTwo;

    private static BufferedReader readerOne;

    private static BufferedReader readerTwo;

    private static State state;

    public static final String[] EMPTY_ROW = new String[0];


    public static void main(String[] args) throws IOException {

        try {

            /**
             * First read in command line arguments/options.
             * We assume that there are two logfiles as input
             * each of them containing rows
             * ACQUIRE \delimiter RESOURCE or
             * RELEASE \delimiter RESOURCE
             * with an optionally given delimiter -d (defaults to ",")
             */
            Options options = new Options();
            options.addOption("d", true, "delimiter");
            options.addOption("v",false,"verbose");

            CommandLineParser parser = new GnuParser();
            CommandLine cmd = parser.parse(options, args);

            DELIMITER = cmd.hasOption("d") ? cmd.getOptionValue("d") : ",";

            if(cmd.hasOption("v")) {
                logger.setLevel(Level.DEBUG);
            }

            String[] logFiles = cmd.getArgs();
            if(logFiles.length == 2) {
                logFileOne = logFiles[0];
                logFileTwo = logFiles[1];
            } else {
                throw new IllegalArgumentException("Please give exactly two logfiles with absolute file path names...");
            }

            /**
             * We use two BufferedReaders for the two input logfiles
             * We initialize a State object that realizes and manages the whole resource allocation process
             * we start a while loop until the State object decides not to continue it
             * because either a DEADLOCK or a SELFLOCK was found
             * or the two threads could be FINISHED without finding the above two LOCKS
             *
             */
            readerOne = new BufferedReader(new FileReader(logFileOne));
            readerTwo = new BufferedReader(new FileReader(logFileTwo));
            state = new State();
            boolean stillContinue = true;
            int iter = 0;

            while(stillContinue) {

                /**
                 * In each iteration we call the processState method of the State object
                 * If there is still lines to read in from either logfiles, these are passed to the processState method.
                 *
                 * The method gives back the current state of the resource allocation process.
                 * There is either found a DEADLOCK or a SELFLOCK
                 * or both threads are finished without finding any of the above locks
                 * or the threads are not yet finished and the process will CONTINUE
                 *
                 * The State object does not verify the validity of the logfiles, i.e.
                 * whether the given series of allocation events are realizable or not, except
                 * if there is a malformed row in any of the logfiles an Exception is thrown
                 */
                String lineOne = readerOne.readLine();
                String lineTwo = readerTwo.readLine();

                String[] lineArrOne = lineOne == null ? EMPTY_ROW : lineOne.split(DELIMITER);
                String[] lineArrTwo = lineTwo == null ? EMPTY_ROW : lineTwo.split(DELIMITER);

                if((lineOne != null && lineArrOne.length != 2) || (lineTwo != null && lineArrTwo.length != 2)) {

                    throw new IllegalArgumentException("There is either delimiter problem or a malformed row...");
                }

                state.processState(lineArrOne, lineArrTwo);

                switch (state.getCurrentSate()) {
                    case DEADLOCK:
                        logger.info("DEADLOCK was found...");
                        logger.debug(state);
                        stillContinue = false;
                        break;
                    case SELFLOCK:
                        logger.info("SELFLOCK was found...");
                        logger.debug(state);
                        stillContinue = false;
                        break;
                    case FINISHED:
                        logger.info("FINISHED AND NO DEADLOCK...");
                        stillContinue = false;
                        break;
                    case CONTINUE:
                        logger.debug("CONTINUE RESOURCE ALLOCATION PROCESS " + iter++);
                        break;
                }
            }
        } catch (FileNotFoundException e) {
            logger.error("File not found: please add two complete paths to the logfiles..." + e.getMessage());
        } catch (ParseException e) {
            logger.error("command line arguments are false, use -d for delimiter..." + e.getMessage());
        } catch (IllegalStateException e) {
            logger.error("illegal resource allocation state found..." + e.getMessage());
        } finally {
            if(readerOne != null) {
                readerOne.close();
            }
            if(readerTwo != null) {
                readerTwo.close();
            }
        }
    }
}
