/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 5/27/14
 * Time: 4:14 PM
 */
public enum CurrentState {

    DEADLOCK, SELFLOCK, FINISHED, CONTINUE
}
