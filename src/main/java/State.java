import org.apache.log4j.Logger;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 5/27/14
 * Time: 1:09 PM
 *
 *
 */
public class State {

    private ThreadNode nodeOne;

    private ThreadNode nodeTwo;

    private CurrentState currentState;

    private static final String ACQUIRE = "ACQUIRE";

    private static final String RELEASE = "RELEASE";


    public State() {
        this.nodeOne = new ThreadNode("One");
        this.nodeTwo = new ThreadNode("Two");
    }

    @Override
    public String toString() {
        return "State{\n" +
                "nodeOne= " + nodeOne + "\n" +
                "nodeTwo= " + nodeTwo +
                '}';
    }

    /**
     *
     * @param lineArrOne
     * @param lineArrTwo
     * @return CurrentState
     */
    public void processState(String[] lineArrOne, String[] lineArrTwo) {

        /**
         * In each iteration we check the status of the nodes
         * if both are blocked, or one is locking itself or both are finished then update CurrentState and return
         * else currentState is CONTINUE
         */

        this.currentState = CurrentState.CONTINUE;

        if(this.nodeOne.isBlocked() && this.nodeTwo.isBlocked()) {

            this.currentState = CurrentState.DEADLOCK;
            return;
        }

        if(nodeOne.isFinished() && nodeTwo.isFinished()) {

            this.currentState = CurrentState.FINISHED;
            return;
        }

        if(nodeOne.isSelfLocked() || nodeTwo.isSelfLocked()) {

            this.currentState = CurrentState.SELFLOCK;
            return;
        }

        /**
         * update the states of the nodes
         */
        ProcessLogs.logger.debug("processing nodes...");

        this.processNode(this.nodeOne, lineArrOne, this.nodeTwo.getAcquiredResources());
        this.processNode(this.nodeTwo, lineArrTwo, this.nodeOne.getAcquiredResources());

        ProcessLogs.logger.debug(nodeOne);
        ProcessLogs.logger.debug(nodeTwo);
    }

    /**
     *
     * @param node
     * @param nextLineArr
     * @param otherAcquiredResources
     */
    private void processNode(ThreadNode node, String[] nextLineArr, HashSet<String> otherAcquiredResources) throws IllegalStateException {

        /**
         * if there is still new lines in the logfile, than update line queue
         * if node is not blocked then process with next line from the line queue
         * else check blocking
         */
        if(!nextLineArr.equals(ProcessLogs.EMPTY_ROW)) {
            node.updateLines(nextLineArr);
        }
        if(!node.isBlocked()) {
            String[] lineArr = node.getNextLine();
            if(lineArr == null) {
                node.finish();
                return;
            }
            switch (lineArr[0]) {
                case ACQUIRE:
                    if(!otherAcquiredResources.contains(lineArr[1])) {
                        node.addResource(lineArr[1]);
                    } else {
                        node.block(lineArr[1]);
                    }
                    break;
                case RELEASE:
                    node.releaseResource(lineArr[1]);
                    break;
                default:
                    throw new IllegalStateException("Logfile has unknown action...It should be either ACQUIRE or RELEASE!");
            }
        } else {
            if(!otherAcquiredResources.contains(node.getBlockedBy())) {
                node.releaseBlock();
                node.addResource(node.getBlockedBy());
                node.resetBlockedBy();
            }
        }
    }

    public CurrentState getCurrentSate() {
        return this.currentState;
    }

}
