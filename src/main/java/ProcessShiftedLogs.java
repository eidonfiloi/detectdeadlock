import org.apache.commons.cli.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by eidonfiloi on 29/05/14.
 */
public class ProcessShiftedLogs {

    public static final Logger logger = Logger.getLogger(ProcessShiftedLogs.class);

    private static final String[] EMPTY_ROW = new String[0];

    protected static final String ACQUIRE = "ACQUIRE";

    protected static final String RELEASE = "RELEASE";

    protected static String DELIMITER;

    private static String logFileOne;

    private static String logFileTwo;

    private static ArrayList<HashSet<String>> seriesOne;

    private static ArrayList<HashSet<String>> seriesTwo;

    private static BufferedReader readerOne;

    private static BufferedReader readerTwo;


    public static void main(String[] args) throws IOException {

        try {
            Options options = new Options();
            options.addOption("d", true, "delimiter");
            options.addOption("v",false,"verbose");

            CommandLineParser parser = new GnuParser();
            CommandLine cmd = parser.parse(options, args);

            DELIMITER = cmd.hasOption("d") ? cmd.getOptionValue("d") : ",";

            if(cmd.hasOption("v")) {
                logger.setLevel(Level.DEBUG);
            }

            String[] logFiles = cmd.getArgs();
            if(logFiles.length == 2) {
                logFileOne = logFiles[0];
                logFileTwo = logFiles[1];
            } else {
                throw new IllegalArgumentException("Please give exactly two logfiles with absolute file path names...");
            }

            readerOne = new BufferedReader(new FileReader(logFileOne));
            readerTwo = new BufferedReader(new FileReader(logFileTwo));

            seriesOne = constructSeries(readerOne);
            seriesTwo = constructSeries(readerTwo);

            logger.debug(seriesOne);
            logger.debug(seriesTwo);

            boolean foundDeadlock = searchForBadPairs(seriesOne,seriesTwo);

            if(foundDeadlock) {
                logger.info("DEADLOCK WAS FOUND...");
            } else {
                logger.info("FINISHED AND NO DEADLOCK...");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if(readerOne != null) {
                readerOne.close();
            }
            if(readerTwo != null) {
                readerTwo.close();
            }
        }
    }

    /**
     * deadlock is found if there exists two subsequent state pairs (C1-N1 and C2-N2) of each threads such that
     * N1 is one element bigger than C1 and N2 is one element bigger than C2 (i.e. both threads want to acquire)
     * C1 and C2 are disjoint (i.e. it is possible that threadOne and threadTwo are in states C1 and C2 at the same time)
     * N1 and N2 have non trivial intersection (i.e. the threads will be blocked)
     * N1 minus C1 is in C2 and N2 minus C2 is in C1 (i.e. the threads block each other simultaniously)
     *
     * @param arrOne array of subsequent states of thread One - represented by sets of currently acquired resources
     * @param arrTwo array of subsequent states of thread Two - represented by sets of currently acquired resources
     * @return
     */
    protected static boolean searchForBadPairs(ArrayList<HashSet<String>> arrOne, ArrayList<HashSet<String>> arrTwo) {


        int lenOne = arrOne.size();
        int lenTwo = arrTwo.size();
        for(int iterOne = 0; iterOne < lenOne - 1; iterOne++) {
            for(int iterTwo = 0; iterTwo < lenTwo - 1; iterTwo++) {

                HashSet<String> currentOne = arrOne.get(iterOne);
                HashSet<String> currentTwo = arrTwo.get(iterTwo);
                HashSet<String> nextOne = arrOne.get(iterOne + 1);
                HashSet<String> nextTwo = arrTwo.get(iterTwo + 1);

                if(nextOne.size() - currentOne.size() != 1 || nextTwo.size() - currentTwo.size() != 1) {
                    continue;
                }

                HashSet<String> intersectionCurrent = new HashSet<String>(currentOne);
                intersectionCurrent.retainAll(currentTwo);

                HashSet<String> intersectionNext = new HashSet<String>(nextOne);
                intersectionNext.retainAll(nextTwo);

                if(!intersectionNext.isEmpty() && intersectionCurrent.isEmpty()) {

                    HashSet<String> differenceOne = new HashSet<String>(nextOne);
                    differenceOne.removeAll(currentOne);

                    HashSet<String> differenceTwo = new HashSet<String>(nextTwo);
                    differenceTwo.removeAll(currentTwo);

                    if(currentTwo.containsAll(differenceOne) && currentOne.containsAll(differenceTwo)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected static ArrayList<HashSet<String>> constructSeries(BufferedReader reader) throws IOException, IllegalArgumentException, IllegalStateException {

        /**
         * we process each logfile step by step
         * at each step keep track of the current set of acquired resources
         * create an array of these sets
         */
        ArrayList<HashSet<String>> result = new ArrayList<HashSet<String>>();
        HashSet<String> last = new HashSet<String>();
        result.add(new HashSet<String>(last));
        String line;
        while((line = reader.readLine()) != null) {

            String[] lineArr = line.split(DELIMITER);

            if(lineArr.length != 2) {
                throw new IllegalArgumentException("There is either delimiter problem or a malformed row...");
            }

            if(lineArr[0].equals(ACQUIRE)) {
                boolean canBeAdded = last.add(lineArr[1]);
                if(!canBeAdded) {
                    throw new IllegalStateException("SELFLOCK was found...");
                }
            } else if(lineArr[0].equals(RELEASE)) {
                last.remove(lineArr[1]);
            } else {
                throw new IllegalStateException("Logfile has unknown action...It should be either ACQUIRE or RELEASE!");
            }

            result.add(new HashSet<String>(last));

        }

        reader.close();

        return result;

    }
}
