import java.util.ArrayDeque;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 5/27/14
 * Time: 1:34 PM
 */
public class ThreadNode {

    private static final String NOTHING = "NOTHING";

    private HashSet<String> acquiredResources;

    private ArrayDeque<String[]> lineQueue;

    private boolean isBlocked;

    private String blockedBy;

    private boolean isFinished;

    private boolean isSelfLocked;

    private String name;

    public ThreadNode(String name) {

        this.name = name;
        this.acquiredResources = new HashSet<String>();
        this.lineQueue = new ArrayDeque<String[]>();
        this.isBlocked = false;
        this.isFinished = false;
        this.isSelfLocked = false;
        this.blockedBy = NOTHING;
    }

    @Override
    public String toString() {
        return "ThreadNode " + this.name +
                ", blocked: " + this.isBlocked() +
                ", finished: " + this.isFinished() +
                ", blocked by: " + this.blockedBy +
                ", acquired resources:" + this.acquiredResources.toString();
    }

    /**
     *
     * @param nextLineArr
     */
    public void updateLines(String[] nextLineArr) {

        if(nextLineArr.length == 2) {
            this.lineQueue.addFirst(nextLineArr);
        }
    }

    public String[] getNextLine() {
        return this.lineQueue.pollLast();
    }

    public void addResource(String resource) {

        boolean canBeAdded = this.acquiredResources.add(resource);
        if(!canBeAdded) {
            this.isSelfLocked = true;
            this.isBlocked = true;
            this.blockedBy = resource;
        }
    }

    public void releaseResource(String resource) {
        this.acquiredResources.remove(resource);
    }

    public boolean isBlocked() {
        return new Boolean(this.isBlocked);
    }

    public void block(String blockedBy) {
        this.isBlocked = true;
        this.blockedBy = blockedBy;
    }

    public void releaseBlock() {
        this.isBlocked = false;
    }

    public boolean isFinished() {
        return new Boolean(this.isFinished);
    }

    public void finish() {
        this.isFinished = true;
        this.acquiredResources.clear();
    }

    public boolean isSelfLocked() {
        return new Boolean(this.isSelfLocked);
    }

    public String getBlockedBy() {
        return blockedBy;
    }

    public void resetBlockedBy() {
        this.blockedBy = NOTHING;
    }

    public HashSet<String> getAcquiredResources() {
        return new HashSet<String>(this.acquiredResources);
    }
}
